# build environment
FROM node:12 as builder
RUN mkdir /usr/src/app
COPY package.json /usr/src/app
COPY yarn.lock /usr/src/app
WORKDIR /usr/src/app
RUN yarn install
COPY . /usr/src/app
RUN yarn build

# production environment
FROM node:12-alpine
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/package.json /usr/src/app/package.json
COPY --from=builder /usr/src/app/index.js /usr/src/app/index.js
COPY --from=builder /usr/src/app/build /usr/src/app/build
RUN npm install express
EXPOSE 3000
CMD [ "npm", "run", "start" ]
